#include <string>
#include <vector>
#include <map>
#include <cstdlib>

class CommandParser
{
  private:

    typedef std::vector<std::string> Tokens;

    Tokens static split(const std::string& strcommand)
    {
      Tokens tokens;
      size_t idx = 0;
      size_t jdx;
      while (idx < strcommand.length())
      {
        size_t jdx = strcommand.find_first_of(' ',idx);
        if (jdx > strcommand.length())
        {
          jdx = strcommand.length();
          if (strcommand[strcommand.length()-1] == '\n')
            jdx -= 1;
        }

        tokens.push_back(strcommand.substr(idx,jdx-idx));
        idx = jdx+1;

        /* Just in case there is more than one space */
        while (idx < strcommand.length() and strcommand[idx] == ' ')
          ++idx;
      }
      return tokens;
    }

  public:

    int static const SET_MOTORS = 0;
    int static const GET_MOTORS = 1;
    int static const GET_LDS_SCAN = 2;
    int static const UNKNOWN = 3;

    std::string command_name;
    int command;
    std::map<std::string,double> parameters;

    CommandParser(const std::string& strcommand)
    {
      Tokens tokens = split(strcommand);
      if (tokens.empty()) 
      {
        command = UNKNOWN;
      }
      else if (tokens[0] == "GetLDSScan")
      {
        command = GET_LDS_SCAN;
      }
      else if (tokens[0] == "SetMotor")
      {
        command = SET_MOTORS;
        for (int idx = 1; idx < tokens.size()-1; idx += 2)
        {
          parameters[tokens[idx]] = atof(tokens[idx+1].c_str());
        }
      }
      else if (tokens[0] == "GetMotors")
      {
        command = GET_MOTORS;
        for (int idx = 1; idx < tokens.size(); ++idx)
        {
          parameters[tokens[idx]] = 1;
        }
      }
      else
      {
        command = UNKNOWN;
      }

      if (not tokens.empty()) command_name = tokens[0];
    }
};

