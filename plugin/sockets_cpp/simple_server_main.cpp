#include "NonBlockingServerSocket.hpp"
#include "SocketException.h"
#include <iostream>
#include <string>
#include <cstdlib>
#include <assert.h>

int main (int argc, char** argv)
{
  std::cout << "running....\n";

  assert(argc == 2 and "Usage: simple_server_main portno");

  try
  {
    // Create the socket
    NonBlockingServerSocket server(atoi(argv[1]));

    while (true)
    {
      NonBlockingServerSocket new_sock;
      while (not server.accept (new_sock))
      {
        std::cout << "Could not accept connection this time. Waiting..."
          << std::endl;
        sleep(1);
      }

      std::cout << "Great! New connection accepted." << std::endl;

      try
      {
        std::string data;
        int read;
        while ((read = new_sock.readline(data)) != 0)
        {
          if (read > 0)
          {
            std::cout << "I have received this: " << data;
            new_sock.writeline("Received: "+data);
          }
          else 
          {
            std::cout << "Could not read anything this time. Waiting..."
              << std::endl;
            sleep(1);
          }
        }
      }
      catch ( SocketException& e)
      {
        std::cout << e.description() << std::endl;
      }
    }
  }
  catch ( SocketException& e )
  {
    std::cout << "Exception was caught:" << e.description() << "\nExiting.\n";
  }

  return 0;
}
