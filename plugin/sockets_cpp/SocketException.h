// SocketException class


#ifndef SocketException_class
#define SocketException_class

#include <string>
#include <cstring>
#include <errno.h>

class SocketException
{
public:

  SocketException() : m_s(strerror(errno)),code_(errno) {}
  SocketException ( std::string s, int code=0 ) : m_s ( s ),code_(code) {}
  ~SocketException () {};

  std::string description()
  {
    return m_s;
  }

  int code()
  {
    return code_;
  }

private:

  std::string m_s;
  int code_;

};

#endif
