// Implementation of the NonBlockingServerSocket class

#include "NonBlockingServerSocket.hpp"
#include "SocketException.h"
#include <cstring>
#include <iostream>


bool NonBlockingServerSocket::retrieve_line(std::string& line)
{
  bool retrieved = false;
  std::size_t idx = accumulator_.find_first_of('\n');
  if (idx != std::string::npos)
  {
    line = accumulator_.substr(0,idx+1);
    accumulator_.erase(0,idx+1);
    retrieved = true;
  }
  return retrieved;
}

NonBlockingServerSocket::NonBlockingServerSocket (int port)
{
  if (not Socket::create() or not Socket::bind(port) or not Socket::listen())
  {
    throw SocketException();
  }

  Socket::set_non_blocking(true);
}

NonBlockingServerSocket::~NonBlockingServerSocket() {}

int NonBlockingServerSocket::readline(std::string& line)
{
  int linelen = -1;
  bool retrieved = retrieve_line(line);
  if (not retrieved)
  {
    std::string append;
    int read = Socket::recv(append);
    if (read < 0 and errno != EAGAIN and errno != EWOULDBLOCK)
    {
      throw SocketException();
    }
    else if (read == 0)
    {
      accumulator_.clear();
      linelen = 0;
    }
    else if (read > 0)
    {
      accumulator_ += append;
      retrieved = retrieve_line(line);
    }
  }
  if (retrieved) linelen = line.length();
  return linelen;
}

void NonBlockingServerSocket::writeline(const std::string& line)
{
  bool sent;
  if (not line.empty() and line[line.length()-1] == '\n')
  {
    sent = Socket::send(line);
  }
  else
  {
    sent = Socket::send(line+'\n');
  }
  if (not sent)
  {
    throw SocketException();
  }
}

bool NonBlockingServerSocket::accept(NonBlockingServerSocket& sock)
{
  bool accepted = Socket::accept(sock);
  if (not accepted and errno != EAGAIN and errno != EWOULDBLOCK)
  {
    throw SocketException();
  }
  return accepted;
}

NonBlockingServerSocket::operator bool()
{
  if (accumulator_.empty())
  { 
    std::string append;
    int read = Socket::recv(append);
    if (read > 0)
    {
      accumulator_ += append;
    }
    else if (errno != EAGAIN and errno != EWOULDBLOCK)
    {
      throw SocketException();
    }
  }
  return not accumulator_.empty();
}
