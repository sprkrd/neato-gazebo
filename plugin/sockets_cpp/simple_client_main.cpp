#include "ClientSocket.h"
#include "SocketException.h"
#include <iostream>
#include <string>
#include <cstdlib>

int main (int, char** argv)
{
  try
  {

    ClientSocket client_socket ( "localhost", atoi(argv[1]) );

    try
    {
      while (std::cin)
      {
        std::string command,reply;
        std::getline(std::cin,command);
        client_socket << command << "\n";
        client_socket >> reply;
        std::cout << "Response: " << reply;
      }
    }
    catch (SocketException& ex)
    {
      std::cerr << ex.description() << '\n';
    }
  }
  catch ( SocketException& e )
  {
    std::cout << "Exception was caught:" << e.description() << "\n";
  }

  return 0;
}
