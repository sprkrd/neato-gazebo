// Implementation of the ServerSocket class

#include "ServerSocket.h"
#include "SocketException.h"
#include <cstring>
#include <iostream>


bool ServerSocket::retrieve_line(std::string& line)
{
  std::size_t idx = accumulator_.find_first_of('\n');
  if (idx != std::string::npos)
  {
    line = accumulator_.substr(0,idx+1);
    accumulator_.erase(0,idx+1);
    return true;
  }
  return false;
}

ServerSocket::ServerSocket ( int port )
{
  if ( ! Socket::create() )
  {
    throw SocketException ( "Could not create server socket." );
  }

  if ( ! Socket::bind ( port ) )
  {
    throw SocketException ( "Could not bind to port." );
  }

  if ( ! Socket::listen() )
  {
    throw SocketException ( "Could not listen to socket." );
  }

}

ServerSocket::~ServerSocket()
{
}


const ServerSocket& ServerSocket::operator << ( const std::string& s ) const
{
  if ( ! Socket::send ( s ) )
  {
    throw SocketException ( "Could not write to socket." );
  }

  return *this;

}

const ServerSocket& ServerSocket::operator >> ( std::string& s ) const
{
  if ( ! Socket::recv ( s ) )
  {
    throw SocketException ( "Could not read from socket." );
  }

  return *this;
}

bool ServerSocket::readline(std::string& line)
{
  if (retrieve_line(line)) return true;
  std::string append;
  int read = Socket::recv(append,MSG_DONTWAIT);
  if (not read and errno != EAGAIN and errno != EWOULDBLOCK)
  {
    throw SocketException(strerror(errno));
  }
  accumulator_ += append;
  return retrieve_line(line);
}

void ServerSocket::accept ( ServerSocket& sock )
{
  if ( ! Socket::accept ( sock ) )
  {
    throw SocketException ( "Could not accept socket." );
  }
}
