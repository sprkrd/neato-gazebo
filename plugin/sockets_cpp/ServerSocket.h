// Definition of the ServerSocket class

#ifndef ServerSocket_class
#define ServerSocket_class

#include "Socket.h"
#include <string>


class ServerSocket : private Socket
{

private:
  std::string accumulator_;

  bool retrieve_line(std::string& line);

public:

  ServerSocket ( int port );
  ServerSocket () {};
  virtual ~ServerSocket();

  const ServerSocket& operator << ( const std::string& ) const;
  const ServerSocket& operator >> ( std::string& ) const;

  bool readline(std::string& line);

  void accept ( ServerSocket& );

};


#endif
