// Definition of the NonBlockingServerSocket class

#ifndef NonBlockingServerSocket_class
#define NonBlockingServerSocket_class

#include "Socket.h"
#include <string>


class NonBlockingServerSocket : private Socket
{

private:
  std::string accumulator_;

  bool retrieve_line(std::string& line);

public:

  NonBlockingServerSocket(int port);
  NonBlockingServerSocket() {};
  virtual ~NonBlockingServerSocket();

  const NonBlockingServerSocket& operator<<(const std::string&) const;
  NonBlockingServerSocket& operator>>(std::string&);

  /**
   *  \brief Line reader
   *
   *  Reads a single line (i.e. until a \n character) from the socket. It
   *  makes use of an internal accumulator to store the available characters.
   *
   *  \param line If there is a line available, it stores such line here.
   *
   *  \return an integer < 0 if there is not any available input yet (i.e. the
   *  reading operation would block); 0 if the connection has been
   *  successfully closed; a positive integer indicating the number of
   *  characters of the new line otherwise
   *
   *  \throws SocketException If an error other than EAGAIN or EWOULDBLOCK
   *  occurs, it throws an exception with the adequate error message and
   *  errno code.
   */
  int readline(std::string& line);

  void writeline(const std::string& line);

  /**
   *  \brief Accept connection
   *
   *  Non-blocking operation whose purpose is to accept a new connection.
   *
   *  \param socket Socket that will represent the client
   *
   *  \return true if a new connection could be stablished. false otherwise.
   *
   *  \throws SocketException If an error other than EAGAIN or EWOULDBLOCK
   *  occurs, it throws an exception with the adequate error message and
   *  errno code.
   */
  bool accept(NonBlockingServerSocket& socket);

  /**
   *  \brief Data availability checker.
   *
   *  Checks if there is data available.
   *
   *  \return true if there is new data available. false otherwise. If the
   *  method returns true it is not a guarantee that there is a new line
   *  available.
   *
   *  \throws SocketException If an error other than EAGAIN or EWOULDBLOCK
   *  occurs, it throws an exception with the adequate error message and
   *  errno code.
   */
  operator bool();

};


#endif
