#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/sensors/sensors.hh>
#include <gazebo/common/common.hh>
#include <iostream>
#include <sstream>
#include <cmath>
#include <cassert>

#include "SocketException.h"
#include "NonBlockingServerSocket.hpp"
#include "CommandParser.hpp"

namespace gazebo
{
  class NeatoControl : public ModelPlugin
  {
    private:

      struct Wheel
      {
        physics::JointPtr joint;
        double src_j_angle, dst_j_angle;
        double j_angle, unwrap_j_angle;
        bool inline arrived() const
        {
          double inc_from_current = dst_j_angle-unwrap_j_angle;
          double inc_from_src = dst_j_angle-src_j_angle;
          bool different_signs = inc_from_current*inc_from_src < 0;
          bool arrived = std::fabs(inc_from_current) < 0.017 or
            different_signs;
          return arrived;
        }
        bool inline positive_speed() const
        {
          return dst_j_angle > unwrap_j_angle;
        }
      };

      const static int DISCONNECTED = 0;
      const static int CONNECTED = 1;

      bool receive_next_command(std::string& command)
      {
        bool command_read = false;
        try
        {
          if (status == DISCONNECTED)
          {
            if (conn_socket.accept(server_socket))
            {
              std::cerr << "New connection to client!" << "\n";
              status = CONNECTED;
            }
          }
          else /* status = CONNECTED */
          {
            int read = server_socket.readline(command);
            if (read == 0)
            {
              std::cerr << "Connection closed." << "\n";
              status = DISCONNECTED;
            }
            else if (read > 0)
            {
              std::cerr << "New command received: " << command << "\n";
              command_read = true;
            }
          }
        }
        catch (SocketException& ex)
        {
          std::cerr << ex.description() << '\n';
        }
        return command_read;
      }

      void update_angle(Wheel& wheel)
      {
        double j_angle_new = wheel.joint->GetAngle(0).Radian();
        double inc = j_angle_new - wheel.j_angle;
        if (std::fabs(inc) > 0.017)
        {
          if (inc > 0)
          {
            wheel.unwrap_j_angle += inc-2*M_PI;
          }
          else
          {
            wheel.unwrap_j_angle += inc+2*M_PI;
          }
        }
        else wheel.unwrap_j_angle += inc;
        wheel.j_angle = j_angle_new;
      }

      int status;

      NonBlockingServerSocket conn_socket;
      NonBlockingServerSocket server_socket;

      Wheel left_wheel, right_wheel;

      double radius;
      double speed;

      sensors::RaySensorPtr lidar;

      event::ConnectionPtr updateConnection;


    public:
      NeatoControl() : status(DISCONNECTED), conn_socket(50000),
      radius(.03335) {}

      virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
      {
        left_wheel.joint = _parent->GetJoint("left_wheel_hinge");
        right_wheel.joint = _parent->GetJoint("right_wheel_hinge");
        left_wheel.joint->SetMaxForce(0,100);
        right_wheel.joint->SetMaxForce(0,100);

        lidar = boost::dynamic_pointer_cast<sensors::RaySensor>(
            SingletonT<sensors::SensorManager>::Instance()
            ->GetSensor("lidar"));

        if (lidar)
        {
          std::cerr << "lidar found. Parameters:" <<
            "\n  Min angle: " << lidar->GetAngleMin().Degree() <<
            "\n  Max angle: " << lidar->GetAngleMax().Degree() <<
            "\n  Resolution: " << (lidar->GetAngleResolution()*180/M_PI) << "\n";
        }
        else std::cerr << "Could not find ray sensor named lidar\n";

        // Listen to the update event. This event is broadcast every
        // simulation iteration.
        this->updateConnection = event::Events::ConnectWorldUpdateBegin(
            boost::bind(&NeatoControl::OnUpdate, this, _1));
      }

      // Called by the world update start event
      virtual void OnUpdate(const common::UpdateInfo& /*_info*/)
      {
        typedef std::map<std::string,double>::const_iterator Iter;
        update_angle(left_wheel);
        update_angle(right_wheel);

        std::string command;
        if (receive_next_command(command))
        {
          CommandParser parser(command);
          std::cout << parser.command_name << "(" << parser.command << ")"
            << ". Parameters: \n";
          for (Iter it = parser.parameters.begin();
              it != parser.parameters.end();++it)
          {
            std::cout << it->first << ": " << it->second << "\n";
          }
          std::ostringstream oss;
          double angle, res;
          switch(parser.command)
          {
            case CommandParser::SET_MOTORS:
              if (parser.parameters.count("LWheelDist"))
              {
                left_wheel.src_j_angle = left_wheel.unwrap_j_angle;
                left_wheel.dst_j_angle = left_wheel.unwrap_j_angle +
                  parser.parameters["LWheelDist"]/(1000.0*radius);
              }
              if (parser.parameters.count("RWheelDist"))
              {
                right_wheel.src_j_angle = right_wheel.unwrap_j_angle;
                right_wheel.dst_j_angle = right_wheel.unwrap_j_angle +
                  parser.parameters["RWheelDist"]/(1000.0*radius);
              }
              if (parser.parameters.count("Speed"))
              {
                speed = parser.parameters["Speed"]/(1000.0*radius);
              }
              server_socket.writeline("ok");
              break;
            case CommandParser::GET_MOTORS:
              break;
            case CommandParser::GET_LDS_SCAN:
              oss << "##GetLDSScan;AngleInDegrees,DistInMM,Intensity,ErrorCodeHEX";
              angle = lidar->GetAngleMin().Degree();
              res = lidar->GetAngleResolution()*180/M_PI;
              for (int i = 0; i < lidar->GetRayCount(); ++i)
              {
                oss << ";" << (angle+i*res) << "," <<
                  (int)(lidar->GetRange(i)*1000+0.5) << ",1,0";
              }
              oss << ";ROTATION_SPEED,5.1";
              server_socket.writeline(oss.str());
              break;
            default: /* CommandParser::UNKNOWN */
              server_socket.writeline("Unknown command: " + parser.command_name);
          }
        }

        if (not left_wheel.arrived())
        {
          left_wheel.joint->SetVelocity(0,
              left_wheel.positive_speed()? speed : -speed);
        }
        else
        {
          left_wheel.joint->SetVelocity(0,0);
        }

        if (not right_wheel.arrived())
        {
          right_wheel.joint->SetVelocity(0,
              right_wheel.positive_speed()? speed : -speed);
        }
        else
        {
          right_wheel.joint->SetVelocity(0,0);
        }

      }
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(NeatoControl)
}
